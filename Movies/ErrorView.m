//
//  ErrorView.m
//  Movies
//
//  Created by Nikita Borisov on 19.03.17.
//  Copyright © 2017 Nikita Borisov. All rights reserved.
//

#import "ErrorView.h"

@interface ErrorView ()

@property (nonatomic, strong) UILabel* label;

@end

@implementation ErrorView

-(instancetype)init {
    NSAssert(NO, @"use initWithFrame: for initialization");
    return nil;
}

-(instancetype)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        [self p_construct];
    }
    return self;
}


-(void)layoutSubviews {
    [super layoutSubviews];
    
    [_label sizeToFit];
    _label.center = CGPointMake(self.bounds.size.width / 2.0, self.frame.size.height / 2.0);
}

-(void)setText:(NSString *)text {
    _text = text;
    _label.text = _text;
    [self setNeedsLayout];
    [self layoutIfNeeded];
}


#pragma mark - PRIVATE

-(void) p_construct {
    self.backgroundColor = [UIColor whiteColor];
    
    _label = [[UILabel alloc] init];
    _label.textAlignment = NSTextAlignmentCenter;
    _label.font = [UIFont systemFontOfSize:14.f];
    _label.textColor = [UIColor blackColor];
    _label.numberOfLines = 0;
    [self addSubview:_label];
}

@end
