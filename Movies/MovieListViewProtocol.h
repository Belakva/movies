//
//  MovieListViewProtocol.h
//  Movies
//
//  Created by Nikita Borisov on 19.03.17.
//  Copyright © 2017 Nikita Borisov. All rights reserved.
//

#import <Foundation/Foundation.h>

@class MoviePreviewModel;

@protocol MovieListViewProtocol <NSObject>

@required
-(void) showLoading;
-(void) hideLoading;
-(void) displayData:(NSArray<MoviePreviewModel *> *)movies pagingEnabled:(BOOL)pagingEnabled;
-(void) displayErrorWithText:(NSString*)errorText;
-(void) hideError;

@end
