//
//  LoaderView.m
//  TIML
//
//  Created by Nikita Borisov on 21.10.16.
//  Copyright © 2016 Nikita Borisov. All rights reserved.
//

#import "LoaderView.h"

@implementation LoaderView {
    UIActivityIndicatorView* _activityIndicator;
    BOOL _showing;
}

-(instancetype)init {
    if (self = [super init]) {
        self.frame = [UIScreen mainScreen].bounds;
        self.backgroundColor = [UIColor colorWithWhite:1.0 alpha:0.5];
        _activityIndicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
        _activityIndicator.center = self.center;
        [self addSubview:_activityIndicator];
        self.hidden = YES;
    }
    return self;
}

-(void)show {
    self.hidden = NO;
    [_activityIndicator startAnimating];
    _showing = YES;
}

-(void)hide {
    self.hidden = YES;
    [_activityIndicator stopAnimating];
    _showing = NO;
}

-(BOOL)isShowing {
    return _showing;
}

@end
