//
//  MovieInteractor.m
//  Movies
//
//  Created by Nikita Borisov on 19.03.17.
//  Copyright © 2017 Nikita Borisov. All rights reserved.
//

#import "MovieInteractor.h"
#import "MovieFullModel.h"


@interface MovieInteractor ()

@property (nonatomic, strong) MovieFullModel* loadedMovieModel;

@end


@implementation MovieInteractor

-(instancetype)init {
    if (self = [super init]) {
        self.movieService = [[MovieService alloc] init];
    }
    return self;
}

-(void)requestDataForMovieId:(NSString *)movieId {
    self.loadedMovieModel = nil;
    [self.movieService movieById:movieId completion:^(MovieFullModel *movieModel, NSError *error) {
        if (movieModel && !error) {
            self.loadedMovieModel = movieModel;
            [self.output onMovieDataReceived:self.loadedMovieModel];
        } else {
            NSString* errorString = [self p_errorStringFromError:error];
            [self.output onMovieRequesFailedWithError:errorString];
        }
    }];
}


#pragma mark - PRIVATE

-(NSString*) p_errorStringFromError:(NSError*)error {
    return error.code == NSURLErrorNotConnectedToInternet ? @"You are offline" : @"Error loading data";
}


@end
