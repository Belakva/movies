//
//  MovieParser.m
//  Movies
//
//  Created by Nikita Borisov on 18.03.17.
//  Copyright © 2017 Nikita Borisov. All rights reserved.
//

#import "MovieParser.h"
#import "MoviePreviewModel.h"
#import "MovieFullModel.h"
#import "MoviesListViewController.h"

@implementation MovieParser

-(NSArray<MoviePreviewModel *> *)parseMovieListData:(NSDictionary *)movieListData {
    NSMutableArray* result = [NSMutableArray array];
    if ([movieListData[@"Response"] boolValue] == YES) {
        NSArray* moviesDataArray = movieListData[@"Search"];
        for (NSDictionary* itemData in moviesDataArray) {
            MoviePreviewModel* movieModel = [[MoviePreviewModel alloc] init];
            movieModel.title = itemData[@"Title"];
            movieModel.imdbID = itemData[@"imdbID"];
            movieModel.posterPreviewUrl = itemData[@"Poster"] && ![itemData[@"Poster"] isEqualToString:@"N/A"] ?
                itemData[@"Poster"] : nil;
            [result addObject:movieModel];
        }
    }
    
    return result;
}


-(MovieFullModel *)parseMovieData:(NSDictionary *)data {
    MovieFullModel* movieModel = nil;
    
    if ([data[@"Response"] boolValue] == YES) {
        movieModel = [[MovieFullModel alloc] init];
        movieModel.title = data[@"Title"];
        movieModel.imdbID = data[@"imdbID"];
        movieModel.posterUrl = data[@"Poster"] && ![data[@"Poster"] isEqualToString:@"N/A"] ?
            data[@"Poster"] : nil;
        movieModel.genre = data[@"Genre"];
        movieModel.director = data[@"Director"];
        movieModel.writers = data[@"Writer"];
        movieModel.actors = data[@"Actors"];
        movieModel.plot = data[@"Plot"];
        movieModel.rating = data[@"imdbRating"];
    }
    
    return movieModel;
}



@end
