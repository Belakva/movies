//
//  MovieListInteractor.h
//  Movies
//
//  Created by Nikita Borisov on 19.03.17.
//  Copyright © 2017 Nikita Borisov. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MovieListInteractorInput.h"
#import "MovieListInteractorOutput.h"

@class MovieService;

@interface MovieListInteractor : NSObject <MovieListInteractorInput>

@property (nonatomic, weak) id<MovieListInteractorOutput> output;
@property (nonatomic, strong) MovieService* movieService;

@end
