//
//  MovieViewController.h
//  Movies
//
//  Created by Nikita Borisov on 19.03.17.
//  Copyright © 2017 Nikita Borisov. All rights reserved.
//

#import "BaseViewController.h"
#import "MovieViewProtocol.h"
#import "MovieViewListenerProtocol.h"

@interface MovieViewController : BaseViewController <MovieViewProtocol>

@property (nonatomic, strong) id <MovieViewListenerProtocol> presenter;

@end
