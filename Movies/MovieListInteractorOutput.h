//
//  MovieListInteractorOutput.h
//  Movies
//
//  Created by Nikita Borisov on 19.03.17.
//  Copyright © 2017 Nikita Borisov. All rights reserved.
//

#import <Foundation/Foundation.h>

@class MoviePreviewModel;

@protocol MovieListInteractorOutput <NSObject>

@required
-(void) onMoviesReceived:(NSArray<MoviePreviewModel*> *)movies allResults:(BOOL)allResults;
-(void) onMoviesNextPageReceived:(NSArray<MoviePreviewModel*> *)movies allResults:(BOOL)allResults;
-(void) onMoviesRequesFailedWithError:(NSString*)errorText;

@end
