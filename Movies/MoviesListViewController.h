//
//  MoviesListViewController.h
//  Movies
//
//  Created by Nikita Borisov on 18.03.17.
//  Copyright © 2017 Nikita Borisov. All rights reserved.
//

#import "BaseViewController.h"
#import "MovieListViewProtocol.h"
#import "MovieListViewListenerProtocol.h"

@interface MoviesListViewController : BaseViewController <MovieListViewProtocol>

@property (nonatomic, strong) id <MovieListViewListenerProtocol> presenter;

@end
