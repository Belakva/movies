//
//  MovieViewController.m
//  Movies
//
//  Created by Nikita Borisov on 19.03.17.
//  Copyright © 2017 Nikita Borisov. All rights reserved.
//

#import "MovieViewController.h"
#import "ErrorView.h"
#import "ImageLoader.h"
#import "MovieFullModel.h"
#import "LoaderView.h"
#import "NetworkImageView.h"

@interface MovieViewController ()

@property (nonatomic, strong) ErrorView* errorView;
@property (weak, nonatomic) IBOutlet NetworkImageView *posterImageView;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UILabel *genreLabel;
@property (weak, nonatomic) IBOutlet UILabel *directorLabel;
@property (weak, nonatomic) IBOutlet UILabel *ratingLabel;
@property (weak, nonatomic) IBOutlet UILabel *writersLabel;
@property (weak, nonatomic) IBOutlet UILabel *actorsLabel;
@property (weak, nonatomic) IBOutlet UILabel *plotLabel;


@end

@implementation MovieViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self.presenter viewDidLoad];
}

-(void)displayMovieData:(MovieFullModel *)movieModel {
    [self p_prepareUiForMovie:movieModel];
}

-(void)displayErrorWithText:(NSString *)errorText {
    if (_errorView == nil) {
        _errorView = [[ErrorView alloc] initWithFrame:self.view.frame];
        [self.view addSubview:_errorView];
    }
    _errorView.text = errorText;
}

-(void)hideError {
    if (_errorView) {
        [_errorView removeFromSuperview];
        _errorView = nil;
    }
}

-(LoaderView*) createLoader {
    LoaderView* loader = [[LoaderView alloc] init];
    loader.backgroundColor = [UIColor whiteColor];
    return loader;
}

#pragma mark - PRIVATE

-(void) p_prepareUiForMovie:(MovieFullModel*)movieModel {
    self.titleLabel.text = movieModel.title;
    self.genreLabel.text = movieModel.genre;
    self.directorLabel.text = movieModel.director;
    self.ratingLabel.text = [NSString stringWithFormat:@"%@ / 10.0", movieModel.rating];
    self.writersLabel.text = [NSString stringWithFormat:@"Writers: %@", movieModel.writers];
    self.actorsLabel.text = [NSString stringWithFormat:@"Actors: %@", movieModel.actors];
    self.plotLabel.text = movieModel.plot;
    
    [self.posterImageView setImageWithUrl:movieModel.posterUrl];
}


@end
