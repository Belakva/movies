//
//  ImageLoader.m
//  Movies
//
//  Created by Nikita Borisov on 19.03.17.
//  Copyright © 2017 Nikita Borisov. All rights reserved.
//

#import "ImageLoader.h"

@implementation ImageLoader

+ (ImageLoader *)shared
{
    static ImageLoader* loader;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        loader = [[ImageLoader alloc] init];
        NSURLCache *URLCache = [[NSURLCache alloc] initWithMemoryCapacity:4 * 1024 * 1024
                                                             diskCapacity:20 * 1024 * 1024
                                                                 diskPath:nil];
        [NSURLCache setSharedURLCache:URLCache];
    });
    
    return loader;
}

-(NSURLSessionTask*)downloadImageWithPath:(NSString *)imageUrlPath completion:(ImageDownloadHandler)completion {
    NSAssert(imageUrlPath != nil, @"image url path cannot be nil");
    
    NSURL *url = [NSURL URLWithString:imageUrlPath];
    NSURLSessionDownloadTask *downloadImageTask = [[NSURLSession sharedSession]
                                                   downloadTaskWithURL:url completionHandler:^(NSURL *location, NSURLResponse *response, NSError *error) {
                                                       if (!error) {
                                                           UIImage *downloadedImage = [UIImage imageWithData:
                                                                                       [NSData dataWithContentsOfURL:location]];
                                                           dispatch_async(dispatch_get_main_queue(), ^{
                                                               completion(downloadedImage, error);
                                                           });
                                                       } else {
                                                           dispatch_async(dispatch_get_main_queue(), ^{
                                                               completion(nil, error);
                                                           });
                                                       }
                                                   }];
    
    [downloadImageTask resume];
    return downloadImageTask;
}

@end
