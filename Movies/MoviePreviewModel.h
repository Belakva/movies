//
//  MoviewPreviewModel.h
//  Movies
//
//  Created by Nikita Borisov on 18.03.17.
//  Copyright © 2017 Nikita Borisov. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MoviePreviewModel : NSObject

@property (nonatomic, copy) NSString* title;
@property (nonatomic, copy) NSString* imdbID;
@property (nonatomic, copy) NSString* posterPreviewUrl;

@end
