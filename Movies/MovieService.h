//
//  MoviesService.h
//  Movies
//
//  Created by Nikita Borisov on 18.03.17.
//  Copyright © 2017 Nikita Borisov. All rights reserved.
//

#import <Foundation/Foundation.h>

@class MoviePreviewModel;
@class MovieFullModel;

typedef void(^MovieListRequestHandler)(NSArray<MoviePreviewModel *>* moviesArray, NSError* error);
typedef void(^MovieRequestHandler)(MovieFullModel* movieModel, NSError* error);


@interface MovieService : NSObject

-(void) moviesBySearchString:(NSString*)searchString page:(NSUInteger)page completion:(MovieListRequestHandler)completion;
-(void) movieById:(NSString*)movieId completion:(MovieRequestHandler)completion;

@end
