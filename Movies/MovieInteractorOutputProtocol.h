//
//  MovieInteractorOutputProtocol.h
//  Movies
//
//  Created by Nikita Borisov on 19.03.17.
//  Copyright © 2017 Nikita Borisov. All rights reserved.
//

#import <Foundation/Foundation.h>

@class MovieFullModel;

@protocol MovieInteractorOutputProtocol <NSObject>

@required
-(void) onMovieDataReceived:(MovieFullModel*)movieModel;
-(void) onMovieRequesFailedWithError:(NSString*)errorText;

@end
