//
//  MoviesService.m
//  Movies
//
//  Created by Nikita Borisov on 18.03.17.
//  Copyright © 2017 Nikita Borisov. All rights reserved.
//

#import "MovieService.h"
#import "MovieCache.h"
#import "MovieParser.h"
#import "DataProvider.h"

static NSString* const kBaseUrl = @"http://www.omdbapi.com/?";

@interface MovieService ()

@property (nonatomic, strong) MovieCache* cache;  // в данный момент пуст. Кеширование запилю, если успею.
@property (nonatomic, strong) MovieParser* parser;
@property (nonatomic, strong) id<DataProviderProtocol> dataProvider;

@end


@implementation MovieService

-(instancetype)init {
    if (self = [super init]) {
        self.cache = [[MovieCache alloc] init];
        self.parser = [[MovieParser alloc] init];
        self.dataProvider = [[DataProvider alloc] init];
    }
    return self;
}


-(void)moviesBySearchString:(NSString *)searchString page:(NSUInteger)page completion:(MovieListRequestHandler)completion {
    NSAssert(searchString != nil, @"Search string cannot be nil");
    NSString* urlString = [self p_prepareUrlStringForMovieSearch:searchString page:page];
    [self.dataProvider fetchGETRequest:urlString completion:^(NSDictionary *data, NSError *error) {
        if (!error) {
            NSArray* movies = [self.parser parseMovieListData:data];
            completion(movies, nil);
        } else {
            completion(nil, error);
        }
    }];
}


-(void)movieById:(NSString *)movieId completion:(MovieRequestHandler)completion {
    NSAssert(movieId != nil, @"Movie ID cannot be nil");
    NSString* urlString = [self p_prepareUrlStringForMovieRequest:movieId];
    [self.dataProvider fetchGETRequest:urlString completion:^(NSDictionary *data, NSError *error) {
        if (!error) {
            MovieFullModel* movieModel = [self.parser parseMovieData:data];
            completion(movieModel, nil);
        } else {
            completion(nil, error);
        }
    }];
}


#pragma mark - PRIVATE

-(NSString*) p_prepareUrlStringForMovieSearch:(NSString*)searchString page:(NSUInteger)page {
    NSString* urlString = [NSString stringWithFormat:@"%@s=%@&page=%ld", kBaseUrl, searchString, (unsigned long)page];
    return [urlString stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
}


-(NSString*) p_prepareUrlStringForMovieRequest:(NSString*)movieId {
    NSString* urlString = [NSString stringWithFormat:@"%@i=%@", kBaseUrl, movieId];
    return [urlString stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
}

@end
