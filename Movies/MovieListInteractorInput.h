//
//  MovieListInteractorInput.h
//  Movies
//
//  Created by Nikita Borisov on 19.03.17.
//  Copyright © 2017 Nikita Borisov. All rights reserved.
//

#import <Foundation/Foundation.h>

@class MoviePreviewModel;

@protocol MovieListInteractorInput <NSObject>

@required
-(void) clearResults;
-(void) requestMoviesBySearchString:(NSString*)searchString;
-(void) requestNextPage;
-(MoviePreviewModel*) moviePreviewModelForId:(NSUInteger)rowId;
-(NSArray*) allLoadedMovies;


@end
