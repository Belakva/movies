//
//  NetworkImageView.m
//  Movies
//
//  Created by Nikita Borisov on 19.03.17.
//  Copyright © 2017 Nikita Borisov. All rights reserved.
//

#import "NetworkImageView.h"
#import "ImageLoader.h"

@interface NetworkImageView ()

@property (nonatomic, strong) NSURLSessionTask* downloadTask;

@end

@implementation NetworkImageView

-(void)setImageWithUrl:(NSString *)urlString {
    [self cancelDownloading];

    if (urlString == nil) {
        self.image = nil;
        return;
    }
    
    self.downloadTask = [[ImageLoader shared] downloadImageWithPath:urlString completion:^(UIImage *image, NSError *error) {
        self.image = image;
        self.downloadTask = nil;
    }];
}

-(void)cancelDownloading {
    if (self.downloadTask) {
        [self.downloadTask cancel];
        self.downloadTask = nil;
    }
}


@end
