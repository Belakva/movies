//
//  MovieFullModel.h
//  Movies
//
//  Created by Nikita Borisov on 18.03.17.
//  Copyright © 2017 Nikita Borisov. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MovieFullModel : NSObject

@property (nonatomic, copy) NSString* title;
@property (nonatomic, copy) NSString* imdbID;
@property (nonatomic, copy) NSString* posterUrl;
@property (nonatomic, copy) NSString* genre;
@property (nonatomic, copy) NSString* director;
@property (nonatomic, copy) NSString* writers;
@property (nonatomic, copy) NSString* actors;
@property (nonatomic, copy) NSString* plot;
@property (nonatomic, copy) NSString* rating;

@end
