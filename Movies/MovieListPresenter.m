//
//  MovieListPresenter.m
//  Movies
//
//  Created by Nikita Borisov on 19.03.17.
//  Copyright © 2017 Nikita Borisov. All rights reserved.
//

#import "MovieListPresenter.h"
#import "MoviePreviewModel.h"

@implementation MovieListPresenter

-(void)viewDidLoad {
    
}


-(void)didSelectRow:(NSUInteger)rowInd {
    MoviePreviewModel* movie = [self.interactor moviePreviewModelForId:rowInd];
    if (movie) {
        [self p_moveToMovieScreen:movie.imdbID];
    }
}


-(void)searchQueryStringDidChange:(NSString *)queryStr {
    if (queryStr == nil)
        return;
    
    [self.view showLoading];
    [self.interactor requestMoviesBySearchString:queryStr];
}


-(void)scrollDidReachLastCell {
    [self.interactor requestNextPage];
}


-(void)clearButtonTapped {
    [self.interactor clearResults];
    [self.view hideLoading];
    [self.view hideError];
    [self.view displayData:@[] pagingEnabled:NO];
}


#pragma mark - MOVIE LIST INTERACTOR OUTPUT

-(void)onMoviesReceived:(NSArray<MoviePreviewModel *> *)movies allResults:(BOOL)allResults {
    if (movies.count > 0) {
        [self.view hideError];
        [self.view displayData:movies pagingEnabled:(allResults == NO) ];
    } else {
        [self.view displayErrorWithText:@"We did not find any movies"];
    }
    
    [self.view hideLoading];
}


-(void)onMoviesNextPageReceived:(NSArray<MoviePreviewModel *> *)movies allResults:(BOOL)allResults {
    [self.view displayData:[self.interactor allLoadedMovies] pagingEnabled:(allResults == NO) ];
    [self.view hideLoading];
}


-(void)onMoviesRequesFailedWithError:(NSString *)errorText {
    [self.view displayErrorWithText:errorText];
    [self.view hideLoading];
}


#pragma mark - PRIVATE

-(void) p_moveToMovieScreen:(NSString*)movieId {
    [self.router showMovieScreen:movieId parent:self.view.navigationController];
}

@end
