//
//  Router.h
//  Movies
//
//  Created by Nikita Borisov on 18.03.17.
//  Copyright © 2017 Nikita Borisov. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MovieRouterProtocol.h"

@interface Router : NSObject <MovieRouterProtocol>

-(void) showInitialScreen;
-(void) showMovieScreen:(NSString*)movieId parent:(UINavigationController*)parent;

@end
