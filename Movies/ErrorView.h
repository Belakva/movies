//
//  ErrorView.h
//  Movies
//
//  Created by Nikita Borisov on 19.03.17.
//  Copyright © 2017 Nikita Borisov. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ErrorView : UIView

@property (nonatomic, copy) NSString* text;

@end
