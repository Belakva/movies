//
//  MovieListTableConstructor.m
//  Movies
//
//  Created by Nikita Borisov on 19.03.17.
//  Copyright © 2017 Nikita Borisov. All rights reserved.
//

#import "MovieListTableConstructor.h"
#import "MoviePreviewModel.h"
#import "MovieListCell.h"

@interface MovieListTableConstructor () {
    NSArray* _movies;
    BOOL _pagingEnabled;
}

@end

@implementation MovieListTableConstructor

-(instancetype)initWithDataArray:(NSArray *)dataArray pagingEnabled:(BOOL)pagingEnabled {
    if (self = [super init]) {
        _movies = dataArray;
        _pagingEnabled = pagingEnabled;
    }
    return self;
}

#pragma mark - TABLE STUFF

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString* cellIdentifier = @"movieItemCell";
    static NSString* loadingCellIdentifier = @"loadingCell";
    
    //loading cell
    if (indexPath.row >= _movies.count) {
        UITableViewCell* loadingCell = [tableView dequeueReusableCellWithIdentifier:loadingCellIdentifier forIndexPath:indexPath];
        return loadingCell;
    }
    
    MovieListCell* cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier forIndexPath:indexPath];
    MoviePreviewModel* movieModel = _movies[indexPath.row];
    [cell prepareForMovie:movieModel];
    return cell;
}

-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.row == _movies.count - 1) {
        if (self.delegate && [self.delegate respondsToSelector:@selector(tableConstructorDidReachLastCell:)]) {
            [self.delegate tableConstructorDidReachLastCell:self];
        }
    }
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    // loading cell
    if (indexPath.row >= _movies.count)
        return;
    
    [tableView deselectRowAtIndexPath:indexPath animated:NO];
    if (self.delegate && [self.delegate respondsToSelector:@selector(tableConstructor:didSelectRowAtIndexPath:)]) {
        [self.delegate tableConstructor:self didSelectRowAtIndexPath:indexPath];
    }
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return _pagingEnabled ? _movies.count + 1 : _movies.count;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 70.0;
}


@end
