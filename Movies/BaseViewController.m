//
//  BaseViewController.m
//  Movies
//
//  Created by Nikita Borisov on 18.03.17.
//  Copyright © 2017 Nikita Borisov. All rights reserved.
//

#import "BaseViewController.h"
#import "LoaderView.h"


@interface BaseViewController ()

@property(nonatomic, strong) LoaderView* loader;

@end


@implementation BaseViewController

-(void)showLoading {
    if (!_loader) {
        _loader = [self createLoader];
        [self.view addSubview:_loader];
    }
    [_loader show];
}


-(void)hideLoading {
    if(_loader) {
        [_loader hide];
        [_loader removeFromSuperview];
    }
    self.loader = nil;
}

-(LoaderView*) createLoader {
    return [[LoaderView alloc] init];
}

@end
