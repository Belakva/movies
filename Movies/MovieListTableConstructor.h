//
//  MovieListTableConstructor.h
//  Movies
//
//  Created by Nikita Borisov on 19.03.17.
//  Copyright © 2017 Nikita Borisov. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TableConstructorProtocol.h"


@interface MovieListTableConstructor : NSObject <TableConstructorProtocol>

-(instancetype) initWithDataArray:(NSArray*)dataArray pagingEnabled:(BOOL)pagingEnabled;
@property (nonatomic, weak) id<TableConstructorDelegate> delegate;

@end
