//
//  MoviePresenter.m
//  Movies
//
//  Created by Nikita Borisov on 19.03.17.
//  Copyright © 2017 Nikita Borisov. All rights reserved.
//

#import "MoviePresenter.h"

@implementation MoviePresenter

-(void)viewDidLoad {
    [self.view showLoading];
    [self.interactor requestDataForMovieId:self.movieId];
}


#pragma mark - MOVIE LIST INTERACTOR OUTPUT

-(void)onMovieDataReceived:(MovieFullModel *)movieModel {
    [self.view displayMovieData:movieModel];
    [self.view hideLoading];
}


-(void)onMovieRequesFailedWithError:(NSString *)errorText {
    [self.view displayErrorWithText:errorText];
    [self.view hideLoading];
}


@end
