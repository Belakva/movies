//
//  MoviesListViewController.m
//  Movies
//
//  Created by Nikita Borisov on 18.03.17.
//  Copyright © 2017 Nikita Borisov. All rights reserved.
//

#import "MoviesListViewController.h"
#import "MovieListTableConstructor.h"
#import "ErrorView.h"

@interface MoviesListViewController () <UISearchBarDelegate, TableConstructorDelegate>

@property (nonatomic, weak) IBOutlet UISearchBar* searchBar;
@property (nonatomic, weak) IBOutlet UITableView* tableView;
@property (nonatomic, strong) id<TableConstructorProtocol> tableConstructor;
@property (nonatomic, strong) ErrorView* errorView;

@end

@implementation MoviesListViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self.presenter viewDidLoad];
}


-(void)displayData:(NSArray<MoviePreviewModel *> *)movies pagingEnabled:(BOOL)pagingEnabled {
    _tableConstructor = [[MovieListTableConstructor alloc] initWithDataArray:movies pagingEnabled:pagingEnabled];
    _tableConstructor.delegate = self;
    
    self.tableView.dataSource = _tableConstructor;
    self.tableView.delegate = _tableConstructor;
    [self.tableView reloadData];
    
}


-(void)displayErrorWithText:(NSString *)errorText {
    if (_errorView == nil) {
        _errorView = [[ErrorView alloc] initWithFrame:self.tableView.frame];
        [self.view addSubview:_errorView];
    }
    _errorView.text = errorText;
}


-(void)hideError {
    if (_errorView) {
        [_errorView removeFromSuperview];
        _errorView = nil;
    }
}


#pragma mark - TABLE CONSTRUCTOR DELEGATE

-(void)tableConstructor:(id<TableConstructorProtocol>)tableConstructor didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [self.presenter didSelectRow:indexPath.row];
}


-(void)tableConstructorDidReachLastCell:(id<TableConstructorProtocol>)tableConstructor {
    [self.presenter scrollDidReachLastCell];
}


#pragma mark - UI SEARCH BAR DELEGATE

-(void)searchBarSearchButtonClicked:(UISearchBar *)searchBar {
    [searchBar resignFirstResponder];
    [self.presenter searchQueryStringDidChange:searchBar.text];
}


-(void)searchBarCancelButtonClicked:(UISearchBar *)searchBar {
    [self.presenter clearButtonTapped];
}


@end
