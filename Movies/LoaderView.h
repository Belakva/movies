//
//  LoaderView.h
//  TIML
//
//  Created by Nikita Borisov on 21.10.16.
//  Copyright © 2016 Nikita Borisov. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LoaderView : UIView

-(void)show;
-(void)hide;
-(BOOL)isShowing;

@end
