//
//  DataProviderProtocol.h
//  Movies
//
//  Created by Nikita Borisov on 18.03.17.
//  Copyright © 2017 Nikita Borisov. All rights reserved.
//

#import <Foundation/Foundation.h>

@class UIImage;

typedef void(^RequestHandler)(NSDictionary* data, NSError* error);

@protocol DataProviderProtocol <NSObject>

@required
-(void) fetchGETRequest:(NSString*)urlString completion:(RequestHandler)completion;
-(void) cancelTask:(NSURLSessionTask*)task;
-(void) cancelAllTasks;

@end
