//
//  MovieListCell.m
//  Movies
//
//  Created by Nikita Borisov on 19.03.17.
//  Copyright © 2017 Nikita Borisov. All rights reserved.
//

#import "MovieListCell.h"
#import "MoviePreviewModel.h"
#import "ImageLoader.h"
#import "NetworkImageView.h"

@implementation MovieListCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

-(void)prepareForReuse {
    [super prepareForReuse];
    self.iconImageView.image = nil;
    [self.iconImageView cancelDownloading];
}


-(void)prepareForMovie:(MoviePreviewModel *)movieModel {
    self.titleLabel.text = movieModel.title;
    [self.iconImageView setImageWithUrl:movieModel.posterPreviewUrl];
}

@end
