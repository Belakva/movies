//
//  MovieListCell.h
//  Movies
//
//  Created by Nikita Borisov on 19.03.17.
//  Copyright © 2017 Nikita Borisov. All rights reserved.
//

#import <UIKit/UIKit.h>

@class MoviePreviewModel;
@class NetworkImageView;

@interface MovieListCell : UITableViewCell

@property (nonatomic, weak) IBOutlet NetworkImageView* iconImageView;
@property (nonatomic, weak) IBOutlet UILabel* titleLabel;

-(void) prepareForMovie:(MoviePreviewModel*)movieModel;

@end
