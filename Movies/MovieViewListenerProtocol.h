//
//  MovieViewListenerProtocol.h
//  Movies
//
//  Created by Nikita Borisov on 19.03.17.
//  Copyright © 2017 Nikita Borisov. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol MovieViewListenerProtocol <NSObject>

@required
-(void) viewDidLoad;

@optional
-(void) viewWillDisappear;

@end
