//
//  MovieListViewListenerProtocol.h
//  Movies
//
//  Created by Nikita Borisov on 19.03.17.
//  Copyright © 2017 Nikita Borisov. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol MovieListViewListenerProtocol <NSObject>

@required
-(void) viewDidLoad;
-(void) didSelectRow:(NSUInteger)rowInd;
-(void) scrollDidReachLastCell;
-(void) searchQueryStringDidChange:(NSString*)queryStr;
-(void) clearButtonTapped;

@optional
-(void) viewWillAppear;
-(void) viewDidAppear;
-(void) viewWillDisappear;

@end
