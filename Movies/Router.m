//
//  Router.m
//  Movies
//
//  Created by Nikita Borisov on 18.03.17.
//  Copyright © 2017 Nikita Borisov. All rights reserved.
//

#import "Router.h"
#import "MoviesListViewController.h"
#import "MovieListPresenter.h"
#import "MovieListInteractor.h"

#import "MovieViewController.h"
#import "MoviePresenter.h"
#import "MovieInteractor.h"

@implementation Router


-(void)showInitialScreen {
    UINavigationController* moviesListNavC = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"MoviesListNavController"];
    
    MoviesListViewController* movieListVC =  (MoviesListViewController*)moviesListNavC.topViewController;
    MovieListPresenter* presenter = [[MovieListPresenter alloc] init];
    MovieListInteractor* interactor = [[MovieListInteractor alloc] init];
    
    movieListVC.presenter = presenter;
    presenter.view = movieListVC;
    presenter.interactor = interactor;
    presenter.router = self;
    interactor.output = presenter;
    
    UIWindow* window = [[[UIApplication sharedApplication] delegate] window];
    [window setRootViewController:moviesListNavC];
}


-(void)showMovieScreen:(NSString *)movieId parent:(UINavigationController *)parent {
    MovieViewController* movieVC = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"MovieViewController"];
    MoviePresenter* presenter = [[MoviePresenter alloc] init];
    MovieInteractor* interactor = [[MovieInteractor alloc] init];
    
    movieVC.presenter = presenter;
    presenter.view = movieVC;
    presenter.interactor = interactor;
    presenter.movieId = movieId;
    interactor.output = presenter;
    
    [parent pushViewController:movieVC animated:YES];
}

@end
