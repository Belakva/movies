//
//  MovieListInteractor.m
//  Movies
//
//  Created by Nikita Borisov on 19.03.17.
//  Copyright © 2017 Nikita Borisov. All rights reserved.
//

#import "MovieListInteractor.h"
#import "MovieService.h"

static NSUInteger const kPageSize = 10;

@interface MovieListInteractor () {
    NSUInteger _currentPage;
    NSString* _currentSearchString;
}

@property (nonatomic, strong) NSMutableArray* movies;
@property (nonatomic, assign) BOOL isAllLoaded;
@property (nonatomic, assign) BOOL isPageLoading;


@end

@implementation MovieListInteractor

-(instancetype)init {
    if (self = [super init]) {
        self.movieService = [[MovieService alloc] init];
        _movies = [NSMutableArray array];
    }
    return self;
}


-(void)requestMoviesBySearchString:(NSString *)searchString {
    _currentPage = 1;
    _currentSearchString = searchString;
    _isAllLoaded = NO;
    [_movies removeAllObjects];
    [self.movieService moviesBySearchString:_currentSearchString page:_currentPage completion:^(NSArray<MoviePreviewModel *> *moviesArray, NSError *error) {
        self.isAllLoaded = moviesArray && moviesArray.count < kPageSize;
        if (moviesArray && !error) {
            _movies = [moviesArray mutableCopy];
            [self.output onMoviesReceived:_movies allResults:self.isAllLoaded];
        } else {
            NSString* errorString = [self p_errorStringFromError:error];
            [self.output onMoviesRequesFailedWithError:errorString];
        }
    }];
}


-(void)requestNextPage {
    if (_isAllLoaded || _isPageLoading)
        return;

    NSAssert(_currentSearchString != nil, @"Empty search query string");
    _currentPage += 1;
    _isPageLoading = YES;
    [self.movieService moviesBySearchString:_currentSearchString page:_currentPage completion:^(NSArray<MoviePreviewModel *> *moviesArray, NSError *error) {
        self.isAllLoaded = moviesArray && moviesArray.count < kPageSize;
        self.isPageLoading = NO;
        if (moviesArray && !error) {
            [_movies addObjectsFromArray:moviesArray];
            [self.output onMoviesNextPageReceived:moviesArray allResults:self.isAllLoaded];
        } else {
            NSString* errorString = [self p_errorStringFromError:error];
            [self.output onMoviesRequesFailedWithError:errorString];
        }
    }];
}


-(void)clearResults {
    [_movies removeAllObjects];
    _currentSearchString = nil;
}


-(MoviePreviewModel *)moviePreviewModelForId:(NSUInteger)rowId {
    NSAssert(_movies && rowId < _movies.count , @"Out of range");
    
    return _movies[rowId];
}


-(NSArray *)allLoadedMovies {
    return _movies;
}


#pragma mark - PRIVATE

-(NSString*) p_errorStringFromError:(NSError*)error {
    return error.code == NSURLErrorNotConnectedToInternet ? @"You are offline" : @"Error loading data";
}

@end
