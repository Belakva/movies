//
//  TableConstructorProtocol.h
//  Movies
//
//  Created by Nikita Borisov on 19.03.17.
//  Copyright © 2017 Nikita Borisov. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol TableConstructorProtocol;


@protocol TableConstructorDelegate <NSObject>
@optional
-(void) tableConstructor:(id<TableConstructorProtocol>)tableConstructor didSelectRowAtIndexPath:(NSIndexPath*)indexPath;
-(void) tableConstructorDidReachLastCell:(id<TableConstructorProtocol>)tableConstructor;
@end


@protocol TableConstructorProtocol <NSObject, UITableViewDelegate, UITableViewDataSource>
@property (nonatomic, weak) id<TableConstructorDelegate> delegate;
@end
