//
//  MoviePresenter.h
//  Movies
//
//  Created by Nikita Borisov on 19.03.17.
//  Copyright © 2017 Nikita Borisov. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MovieViewListenerProtocol.h"
#import "MovieInteractorOutputProtocol.h"
#import "MovieInteractorInputProtocol.h"
#import "MovieViewProtocol.h"

@interface MoviePresenter : NSObject <MovieViewListenerProtocol, MovieInteractorOutputProtocol>

@property (nonatomic, weak) UIViewController<MovieViewProtocol>* view;
@property (nonatomic, strong) id<MovieInteractorInputProtocol> interactor;
@property (nonatomic, copy) NSString* movieId;

@end
