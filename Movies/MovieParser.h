//
//  MovieParser.h
//  Movies
//
//  Created by Nikita Borisov on 18.03.17.
//  Copyright © 2017 Nikita Borisov. All rights reserved.
//

#import <Foundation/Foundation.h>

@class MoviePreviewModel;
@class MovieFullModel;


@interface MovieParser : NSObject

-(NSArray<MoviePreviewModel *> *) parseMovieListData:(NSDictionary*)movieListData;
-(MovieFullModel*) parseMovieData:(NSDictionary*)data;

@end
