//
//  MovieListPresenter.h
//  Movies
//
//  Created by Nikita Borisov on 19.03.17.
//  Copyright © 2017 Nikita Borisov. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MovieListViewListenerProtocol.h"
#import "MovieListViewProtocol.h"
#import "MovieListInteractorInput.h"
#import "MovieListInteractorOutput.h"
#import "MovieRouterProtocol.h"

@interface MovieListPresenter : NSObject <MovieListViewListenerProtocol, MovieListInteractorOutput>

@property (nonatomic, weak) UIViewController<MovieListViewProtocol>* view;
@property (nonatomic, strong) id<MovieListInteractorInput> interactor;
@property (nonatomic, strong) id<MovieRouterProtocol> router;

@end
