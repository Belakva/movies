//
//  ImageLoader.h
//  Movies
//
//  Created by Nikita Borisov on 19.03.17.
//  Copyright © 2017 Nikita Borisov. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef void(^ImageDownloadHandler)(UIImage* image, NSError* error);


// На данный момент, простейшая реализация. Предполагается, что данный объект будет прокидовать загруженные картинки
// в кэш-хранилище и доставать их оттуда.
@interface ImageLoader : NSObject

+ (ImageLoader *)shared;
-(NSURLSessionTask*) downloadImageWithPath:(NSString*)imageUrlPath completion:(ImageDownloadHandler)completion;

@end
