//
//  DataProvider.m
//  Movies
//
//  Created by Nikita Borisov on 18.03.17.
//  Copyright © 2017 Nikita Borisov. All rights reserved.
//

#import "DataProvider.h"

@interface DataProvider ()

@end


@implementation DataProvider


-(void)fetchGETRequest:(NSString *)urlString completion:(RequestHandler)completion {
    NSAssert(urlString != nil, @"URL cannot be nil");
    
    NSURL *url = [NSURL URLWithString:urlString];
    NSURLSessionDataTask *task = [[NSURLSession sharedSession]
                                          dataTaskWithURL:url completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                                              if (data && error == nil) {
                                                 NSDictionary *jsonData = [NSJSONSerialization JSONObjectWithData:data options:(NSJSONReadingOptions)0 error:&error];
                                                  dispatch_async(dispatch_get_main_queue(), ^{
                                                      jsonData ? completion(jsonData, nil) : completion(nil, error);
                                                  });
                                              } else {
                                                  dispatch_async(dispatch_get_main_queue(), ^{
                                                      completion(nil, error);
                                                  });
                                              }
                                          }];
    
    [task resume];
}


-(void)cancelTask:(NSURLSessionTask *)task {
    
}


-(void)cancelAllTasks {

}

@end
