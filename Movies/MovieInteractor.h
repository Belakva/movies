//
//  MovieInteractor.h
//  Movies
//
//  Created by Nikita Borisov on 19.03.17.
//  Copyright © 2017 Nikita Borisov. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MovieInteractorInputProtocol.h"
#import "MovieInteractorOutputProtocol.h"
#import "MovieService.h"

@interface MovieInteractor : NSObject <MovieInteractorInputProtocol>

@property (nonatomic, weak) id<MovieInteractorOutputProtocol> output;
@property (nonatomic, strong) MovieService* movieService;

@end
